Docker Ansible Base
==================

[Docker Hub](https://hub.docker.com/r/cvadmins/ansible-base/)
[GitLab](https://gitlab.com/CollegiumV/docker-ansible-base)

The Docker Ansible base container serves as a base container to test Ansible deployment from. Primarily designed for automated testing in a fresh environment, using a CI/CD platform for remote testing and `docker-compose` for local testing. The Docker image also contains a base .yamllint configuration for use with the roles to be tested.

Docker Compose
--------------

`docker-compose` can be used for local testing. Running `docker-compose up --build` will rebuild the ansible-base container and test it with the `test.sh` script.

GitLab CI
---------

GitLabCI is used to build and deploy the container to DockerHub.

1) The container is built and pushed to the internal GitLab container registry.

2) The container is run service, yamllint is tested, and test to verify Ansible can run on the `cvadmins/centos-base` and `cvadmins/voidlinux-base` containers running remotely.

3) If tagged, the container is pushed to Dockerhub.
