FROM centos:7
MAINTAINER cvadmins version: 1.3.0
WORKDIR /root

RUN yum update; yum install -y epel-release; yum update; yum install -y git openssh-clients python2-pip sshpass; yum clean all

ADD requirements.txt .
RUN pip install -r requirements.txt
RUN rm requirements.txt

ADD .yamllint /root/.config/yamllint/config
